const EXPIRES_IN = 60*60*24;
const SECRET = process.env.TOKEN_SECRET || "2d051e884879a8c8456a290abd49e334";
const ALGORITHM = "HS256";
const ISSUER = "lakshithaf@gmail.com";
const AUDIENCE = "lakshithaf@gmail.com";

module.exports.jwtSettings = {
    expiresIn : EXPIRES_IN,
    secret: SECRET,
    algorithm : ALGORITHM,
    issuer : ISSUER,
    audience : AUDIENCE
};