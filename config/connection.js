'use strict';
const mongoose = require('mongoose');

function dbOperation() {
    this.connect = async () => {

        const mangoUrl = 'mongodb://localhost:27017/topbug';
        mongoose.connect(mangoUrl);
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));
        db.once('open', () => {
            console.log('database connected!');
        });
    };
}


module.exports = new dbOperation();
