const router = require ('express').Router();
const rootRoute= require('./moduleRoutes/rootRoute');
const authRoute= require('./moduleRoutes/authRoute');
const customerRoute= require('./moduleRoutes/customerRoute');

router.use('/',authRoute);
router.use('/api/auth/',authRoute);
router.use('/api/customer/',customerRoute);


module.exports = router;



