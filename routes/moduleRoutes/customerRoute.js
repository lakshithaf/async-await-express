'use strict';

const router = require('express').Router();
const apiMap = require('../../api/services/ApiMap');
const commonService = require('../../api/services/CommonService');


router.use(commonService.verifyToken);

router.get('/', async (req, res) => {
    res.send('Hello, World customer!');
});

module.exports = router;