'use strict';

const router = require('express').Router();
const apiMap = require('../../api/services/ApiMap');
const service = require('../../api/services/CommonService');


router.use(service.verifyToken);

router.get('/', async (req, res) => {
    res.send('Welcome to async await api!');
});

router.post('/', async (req, res) => {
    res.send('Welcome to async await api!');
});

module.exports = router;
