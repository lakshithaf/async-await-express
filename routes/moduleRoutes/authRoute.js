'use strict';

const router = require('express').Router();
const apiMap = require('../../api/services/ApiMap');

router.get('/', async (req, res) => {
    res.send('Welcome to async await api!');
});

router.post('/', async (req, res) => {
    res.send('Welcome to async await api!');
});

router.post('/signup', async (req, res) => {
    try {
        const createResult = await apiMap.userController.signUp(req, res);
        if(createResult){
            return res.status(201).json({success: createResult});
        }
        return apiMap.responseHandler.unSuccess(res);

    } catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({error: err.message});
        }
        return apiMap.responseHandler.unSuccess(res);
    }
});

router.post('/login', async (req, res) => {
    try {
        const signInResult = await apiMap.userController.signIn(req, res);
        if(signInResult){
            return res.status(201).json({authToken: signInResult});
        }
        return apiMap.responseHandler.unAuthorizedAccess(res);

    } catch (err) {
        if (err.name === 'ValidationError') {
            return res.status(400).json({error: err.message});
        }
        return apiMap.responseHandler.unSuccess(res);
    }
});

module.exports = router;
