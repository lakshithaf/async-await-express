const UserModel = require('../models/User');
const apiMap = require('../services/ApiMap');
const hashService = require('../services/CipherService');


class UserController {

    static async signUp(req, res) {
        const user = new UserModel();
        user.name = req.body.userName;
        user.email = req.body.email;
        user.password = await hashService.hashPassword(req.body);
        return user.save();

    }

    static async signIn(req, res) {
        const users = await UserModel.findOne({email: req.body.email});
        if (users) {
            const compare = hashService.comparePassword(req.body.password.toString(), users);
            if (compare) {
                const secret = hashService.createToken(users);
                return secret;
            } else {
                return compare;
            }
        } else {
            return apiMap.responseHandler.unAuthorizedAccess(res);
        }
    }

    static async logout(loginData) {

    }

}

module.exports = UserController;