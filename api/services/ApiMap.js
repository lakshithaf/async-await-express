
const apiMapper = {
    userController: require('../controllers/userController'),
    responseHandler: require('../responses/ResponseHandler'),
};

module.exports = apiMapper;
