const cipherService = require('./CipherService');
const apiMap = require('./ApiMap');

module.exports = {
    verifyToken: (req, res, next) => {
        if (!req.headers['x-auth']) {
            return apiMap.responseHandler.tokenMissing(res);
        } else {
            cipherService.verifyToken(req.headers['x-auth'], (err, data) => {
                if (err) {
                    return apiMap.responseHandler.unAuthorizedAccess(res)
                } else {
                    next();
                }
            });

        }
    },
};