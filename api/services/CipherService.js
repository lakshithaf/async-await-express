const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');
const saltRounds = 10;


module.exports = {
    secret: config.jwtSettings.secret,
    issuer: config.jwtSettings.issuer,
    audience: config.jwtSettings.audience,

    /**
     * Hash the password field of the passed user.
     */
    hashPassword: (user) => {
        if (user.password) {
            return new Promise((resolve, reject) => {
                bcrypt.genSalt(saltRounds, (err, salt) => {
                    if (err) {
                        reject(err);
                    } else {
                        bcrypt.hash(user.password.toString(), salt, (err, hash) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve(hash);
                            }

                        });
                    }
                })
            });

        } else {

        }
    },


    /**
     * Compare user password hash with unhashed password
     * @returns boolean indicating a match
     */
    comparePassword: (password, user) => {
        return bcrypt.compareSync(password, user.password);
    },

    /**
     * Create a token based on the passed user
     * @param user
     */
    createToken: (user) => {
        return jwt.sign({
                user: user.toJSON()
            },
            config.jwtSettings.secret,
            {
                algorithm: config.jwtSettings.algorithm,
                expiresIn: config.jwtSettings.expiresIn,
                issuer: config.jwtSettings.issuer,
                audience: config.jwtSettings.audience
            }
        );
    },

    /**
     * Verify a token based on the passed user
     * @param user
     */
    verifyToken: (token, cb) => {
        jwt.verify(
            token,
            config.jwtSettings.secret, {
                algorithm: config.jwtSettings.algorithm,
                expiresIn: config.jwtSettings.expiresIn,
                issuer: config.jwtSettings.issuer,
                audience: config.jwtSettings.audience
            }, (err, decoded) => {
                if (err) {
                    cb(err, undefined);
                } else {
                    cb(undefined, decoded);

                }
            });
    }
};
