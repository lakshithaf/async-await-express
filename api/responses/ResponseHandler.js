'use strict';

function responseHandler (){
    this.accessDeniedMessage = async (res) => {
        res.write(JSON.stringify({
            status: 401,
            message: 'Access denied. Invalid Token'
        }));
        res.send();

    };
    this.unAuthorizedAccess = async (res) => {
        res.writeHead(401, {"Content-Type": "application/json"});
        res.write(JSON.stringify({
            status: 401,
            message: 'Unauthorized'
        }));
        res.send();
    };

    this.tokenMissing = async (res) => {
        res.writeHead(401, {"Content-Type": "application/json"});
        res.write(JSON.stringify({
            status: 401,
            message: 'Token Missing'
        }));
        res.send();
    };

    this.notAvailable = async (res) => {
        res.write(JSON.stringify({
            status: 401,
            message: 'Access denied or invalid resource id'
        }));
        res.send();
    };

    this.success = async (res) => {
        res.write(JSON.stringify({
            status: 200,
            message: 'Successfull'
        }));
        res.send();
    };

    this.unSuccess = async (res) => {
        res.write(JSON.stringify({
            status: 400,
            message: 'Server error'
        }));
        res.send();
    };


    this.invalidFormat = async (res) =>{
        res.write(JSON.stringify({
            status: 402,
            message: 'File format is invalid or file not selected'
        }));
        res.send();
    };

    this.nullData = async (res) => {
        res.write(JSON.stringify({
            status: 404,
            message: 'Data cannot be found'
        }));
        res.send();
    };

    this.dbError = async (res) => {
        res.write(JSON.stringify({
            status: 404,
            message: 'Database error'
        }));
        res.send();
    }

    this.internalError = async (res) => {
        res.write(JSON.stringify({
            status: 500,
            message: 'Internal error'
        }));
        res.send();
    }

};

module.exports = new responseHandler();
