const mongoose = require('mongoose');
/*const validator = require('../services/ModelValidator');*/

const userSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        index: true,
        trim: true,
        lowercase: true,
        email: true,
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'is invalid']
    },
    name: {
        type: String,
        required: true,
        index: true,
    },
    password: {
        type: String,
        required: true,
    }
}, {
    timestamps: true,
});

module.exports = mongoose.model('User', userSchema);