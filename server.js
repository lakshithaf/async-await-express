const express = require('express');
const path = require('path');
const logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const dbConnection = require('./config/connection');
const fs = require('fs');


const router = require('./routes');

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})


const appInit = async () => {
    const app = express();
    dbConnection.connect();
    app.use(logger('combined',{stream: accessLogStream}));
   /* app.use(logger('dev', {
        skip: (req, res) => {
            return res.statusCode < 400
        }
    }));*/
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(cors());
    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });
    app.use('/', router);

    // Handle 404
    app.use((req, res) => {
        res.status(404).send({error: 'Not found'});
    });

    // Handle 500
    app.use((error, req, res, next) => {
        res.status(500).send({error: 'Internal Server Error'});
    });

    app.listen(3000, () => {
        console.log('Dev app listening on port 3000!');
    });


};
appInit().catch(error => console.error(error.stack));
